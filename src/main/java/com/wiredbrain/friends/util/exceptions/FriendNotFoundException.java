package com.wiredbrain.friends.util.exceptions;

public class FriendNotFoundException extends RuntimeException {

    public FriendNotFoundException(Long id) {
        super("Could not find friend " + id);
    }

    public FriendNotFoundException(String firstName, String lastName) {
        super(firstName == null | lastName == null ? "Could not find friend with given name."
                : "Could not find friend " + firstName + " " + lastName);
    }
}
