package com.wiredbrain.friends.model;

public enum Status {

    IN_PROGRESS,
    COMPLETED,
    CANCELLED
}
