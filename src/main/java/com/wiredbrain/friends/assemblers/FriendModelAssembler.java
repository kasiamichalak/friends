package com.wiredbrain.friends.assemblers;

import com.wiredbrain.friends.controller.FriendController;
import com.wiredbrain.friends.model.Friend;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class FriendModelAssembler implements RepresentationModelAssembler<Friend, EntityModel<Friend>> {

    @Override
    public EntityModel<Friend> toModel(Friend entity) {
        return EntityModel.of(entity,
                linkTo(methodOn(FriendController.class).displayFriendById(entity.getId())).withSelfRel(),
                linkTo(methodOn(FriendController.class).readFriends()).withRel("friend"));
    }
}
