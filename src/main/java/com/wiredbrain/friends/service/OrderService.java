package com.wiredbrain.friends.service;

import com.wiredbrain.friends.model.Order;
import org.springframework.data.repository.CrudRepository;

public interface OrderService extends CrudRepository<Order, Long> {
}
