package com.wiredbrain.friends.service;

import com.wiredbrain.friends.model.Friend;
import org.springframework.data.repository.CrudRepository;

public interface FriendService extends CrudRepository<Friend, Long> {

    Iterable<Friend> findByFirstNameAndLastName(String firstName, String lastName);

    Iterable<Friend> findByFirstName(String firstName);

    Iterable<Friend> findByLastName(String lastName);

}
