package com.wiredbrain.friends.controller;

import com.wiredbrain.friends.assemblers.FriendModelAssembler;
import com.wiredbrain.friends.model.Friend;
import com.wiredbrain.friends.service.FriendService;
import com.wiredbrain.friends.util.exceptions.FriendNotFoundException;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
public class FriendController {

    private FriendService friendService;
    private FriendModelAssembler friendModelAssembler;

    public FriendController(FriendService friendService, FriendModelAssembler friendModelAssembler) {
        this.friendService = friendService;
        this.friendModelAssembler = friendModelAssembler;
    }

    @GetMapping("/friend")
    public CollectionModel<EntityModel<Friend>> readFriends() {

        Iterable<Friend> iterable = friendService.findAll();

        List<EntityModel<Friend>> friends = StreamSupport.stream(iterable.spliterator(), false)
                .map(friendModelAssembler::toModel)
                .collect(Collectors.toList());

        return CollectionModel.of(friends, linkTo(methodOn(FriendController.class).readFriends()).withSelfRel());
    }
//add links
    @GetMapping("/friend/{id}")
    public EntityModel<Friend> displayFriendById(@PathVariable Long id) {

        Friend friend = friendService.findById(id)
                .orElseThrow(() -> new FriendNotFoundException(id));

        return friendModelAssembler.toModel(friend);
    }

    // przykładowy url localhost:8080/friend/search?first=Jan&last=Kowalski
    @GetMapping("/friend/search")
    public CollectionModel<EntityModel<Friend>> findFriendByFirstNameAndLastName(@RequestParam(value = "first", required = false) String firstName,
                                                                                 @RequestParam(value = "last", required = false) String lastName) {
        Iterable<Friend> iterable;

        if (firstName != null && lastName != null)
            iterable = friendService.findByFirstNameAndLastName(firstName, lastName);
        else if (firstName != null)
            iterable = friendService.findByFirstName(firstName);
        else if (lastName != null)
            iterable = friendService.findByLastName(lastName);
        else
            iterable = friendService.findAll();

        List<EntityModel<Friend>> friends = StreamSupport.stream(iterable.spliterator(), false)
                .map(friendModelAssembler::toModel)
                .collect(Collectors.toList());

        if(friends.isEmpty()) throw new FriendNotFoundException(firstName, lastName);
        else return CollectionModel.of(friends, linkTo(methodOn(FriendController.class).readFriends()).withSelfRel());
    }

    @PostMapping("/friend")
    public ResponseEntity<?> createFriend(@Valid @RequestBody Friend newFriend) {
        EntityModel<Friend> entityModel = friendModelAssembler.toModel(friendService.save(newFriend));
        return ResponseEntity
                .created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri())
                .body(entityModel);
    }

    @PutMapping("/friend")
    public ResponseEntity<?> updateFriend(@RequestBody Friend updatedFriend) {
        if (friendService.existsById(updatedFriend.getId())) {
            EntityModel<Friend> entityModel = friendModelAssembler.toModel(friendService.save(updatedFriend));
            return ResponseEntity
                    .created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri())
                    .body(entityModel);
        }
        else
            return new ResponseEntity<Friend>(updatedFriend, HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/friend/{id}")
    public ResponseEntity<?> deleteFriend(@PathVariable Long id) {
        friendService.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}
