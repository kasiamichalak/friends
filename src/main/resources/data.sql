insert into friend(first_name, last_name, age, married)
values ('Jan', 'Kowalski', 25, true);
insert into friend(first_name, last_name, age, married)
values ('Barbara', 'Wolska', 34, false);

insert into customer_order(description, status)
values ('MacBook Pro', 'IN_PROGRESS');

insert into customer_order(description, status)
values ('iPhone', 'COMPLETED');